# **Software Design Architecture Mini Project** #

### Training Management System ###

This project is project for subject UTMSPACE SCJ3323 - Software Design and Architecture

### Requirement ###

1. Please open this project in NetBeans. This project develop in NetBeans 8.1 with Glassfish-4.1.1 as webserver

2. Database using MySQL. Development using MySQL version 5.6

### Project Description ###

Each project submission divided by branch such as:

1. Project 1 (project_1): Architectural Style - Design & Implementation. Cover Architectural Design, Detailed Design and **Prototype Implementation**. Architectural Design and Detail Design cover in **Enterprise Architect** (EA) file. This repo only cover for **Prototype Implementation**.
 
2. Project 2 (project_2): Apply design pattern concepts. Continue from Project 1. Chosen design pattern is **Iterator Pattern** for Behavioral Pattern and **Filter Pattern** for Structural Pattern.