<%-- 
    Document   : header
    Created on : Apr 14, 2016, 9:19:18 PM
    Author     : ahmadzulhilmi
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Training System</title>

	<link href="frontend/css/bootstrap.min.css" rel="stylesheet" />
	<link href="frontend/datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" />
	<style>
	    body {
		padding-top: 70px;
	    }
	    .datepicker {
		z-index: 1030 !important;
	    }
	</style>
    </head>
    <body>

	<div class="container">
	    <nav class="navbar navbar-inverse navbar-fixed-top">
		<div class="container-fluid">
		    <!-- Brand and toggle get grouped for better mobile display -->
		    <div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
			    <span class="sr-only">Toggle navigation</span>
			    <span class="icon-bar"></span>
			    <span class="icon-bar"></span>
			    <span class="icon-bar"></span>
			</button>
			<span class="navbar-brand">Training System</span>
		    </div>

		    <!-- Collect the nav links, forms, and other content for toggling -->
		    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
			<ul class="nav navbar-nav">
			    <li><a href="index.jsp">Home</a></li>
			    <li><a href="CourseForm">Add New Course</a></li>
			    <li><a href="CourseCalendarForm">Course/Training Calendar</a></li>
			    <li><a href="AcknowledgeForm">Acknowledge Applications</a></li>
			</ul>
		    </div><!-- /.navbar-collapse -->
		</div><!-- /.container-fluid -->
	    </nav>
	</div>

	<div class="container-fluid">
