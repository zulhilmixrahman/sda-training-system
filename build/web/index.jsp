<%-- 
    Document   : index
    Created on : Apr 14, 2016, 9:18:25 PM
    Author     : ahmadzulhilmi
--%>

<%@include file="inc/header.jsp" %>

<h1 class="text-center">Welcome to Online Integrated Training System</h1>
<hr />
<div class="well-lg text-center">
    <a href="CourseCalendarForm" class="btn btn-primary btn-lg">
	<i class="glyphicon glyphicon-calendar"></i> Course/Training Calendar 
    </a>
</div>

<%@include file="inc/footer.jsp" %>