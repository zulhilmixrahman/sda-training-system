package Application;

import Bussiness.Application;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.Iterator;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author ahmadzulhilmi
 */
public class AcknowledgeForm extends HttpServlet {

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
	    throws ServletException, IOException {
	response.setContentType("text/html;charset=UTF-8");
	try (PrintWriter out = response.getWriter()) {
	    request.getRequestDispatcher("/inc/header.jsp").include(request, response);
	    int applicationID = 0, status = 0;

	    if (request.getParameter("id") != null) {
		applicationID = Integer.parseInt(request.getParameter("id"));
	    }
	    if (request.getParameter("st") != null) {
		status = Integer.parseInt(request.getParameter("st"));
	    }

	    if (applicationID != 0 && status != 0) {
		Application.acknowledgeApplication(applicationID, status);
	    }

	    out.println("<div class=\"col-md-offset-1 col-md-10\">");
	    out.println("<h3>Applications to Acknowledge</h3>");
	    out.println("<hr />");

	    out.println("<div class=\"table-responsive\">");
	    out.println("<table class=\"table table-bordered table-condensed table-hover table-striped\">");
	    out.println("<thead>");
	    out.println("<tr>");
	    out.println("<th class=\"text-center\">#</th>");
	    out.println("<th class=\"text-center\">Course</th>");
	    out.println("<th class=\"text-center\">Staff Name</th>");
	    out.println("<th class=\"text-center\">Date Submited</th>");
	    out.println("<th class=\"col-md-2 text-center\">Action</th>");
	    out.println("</tr>");
	    out.println("</thead>");
	    out.println("<tbody>");

	    int loopItem = 0;
	    Map courseMap = Application.getAllApplicationToAcknowledge();
	    // Loop through records
	    for (Iterator it1 = courseMap.entrySet().iterator(); it1.hasNext();) {
		Map.Entry<Integer, Map> course = (Map.Entry<Integer, Map>) it1.next();
		// Get Array Data
		Map values = course.getValue();
		int acknowledgeStatus = Integer.parseInt(values.get("acknowledgeStatus").toString());
		String acknowledgeCss = "";
		if (acknowledgeStatus == 1) {
		    acknowledgeCss = "success";
		} else if (acknowledgeStatus == 2) {
		    acknowledgeCss = "danger";
		}

		out.println("<tr class=\"" + acknowledgeCss + "\">");
		out.println("<td>" + (loopItem + 1) + "</td>");
		out.println("<td>" + values.get("courseName").toString() + "</td>");
		out.println("<td>" + values.get("staffName").toString() + "</td>");
		out.println("<td>" + values.get("applicationDate").toString() + "</td>");

		if (acknowledgeStatus == 0) {
		    out.println("<td class=\"col-md-3 text-center\">");
		    out.println("<a onClick=\"return confirm('You are AKNOWLEDGE this application?')\" href=\"AcknowledgeForm?id=" + values.get("applicationID").toString() + "&st=1\" class=\"btn btn-success\">Acknowledge</a>");
		    out.println("<a onClick=\"return confirm('You are REJECT this application?')\" href=\"AcknowledgeForm?id=" + values.get("applicationID").toString() + "&st=2\" class=\"btn btn-danger\">Reject</a>");
		    out.println("</td>");
		} else {
		    out.println("<td class=\"text-center\">" + values.get("acknowledgeTxt").toString() + "</td>");
		}

		out.println("</tr>");

		loopItem++;
	    }
	    out.println("</tbody>");
	    out.println("</table>");
	    out.println("</div>");

	    out.println("</div>");

	    request.getRequestDispatcher("/inc/footer.jsp").include(request, response);
	} catch (SQLException ex) {
	    Logger.getLogger(AcknowledgeForm.class.getName()).log(Level.SEVERE, null, ex);
	}
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
	    throws ServletException, IOException {

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
	return "Short description";
    }// </editor-fold>

}
