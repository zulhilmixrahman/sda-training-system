package Application;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import Bussiness.Course;
import java.sql.SQLException;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ahmadzulhilmi
 */
public class ApplicationForm extends HttpServlet {

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
	    throws ServletException, IOException {
	response.setContentType("text/html;charset=UTF-8");
	try (PrintWriter out = response.getWriter()) {
	    request.getRequestDispatcher("/inc/header.jsp").include(request, response);

	    int CourseID = Integer.parseInt(request.getParameter("id"));
	    Map course = Course.getCourseDetails(CourseID);

	    out.println("<div class=\"col-md-offset-1 col-md-10\">");
	    out.println("<h3>Course Details</h3>");
	    out.println("<hr />");

	    out.println("<div class=\"table-responsive\">");
	    out.println("<table class=\"table table-bordered table-condensed table-hover table-striped\">");
	    out.println("<tr><th>Course/Training</th><td>" + course.get("courseName").toString() + "</td></tr>");
	    out.println("<tr><th>Start Date</th><td>" + course.get("courseStartDate").toString() + "</td></tr>");
	    out.println("<tr><th>End Date</th><td>" + course.get("courseEndDate").toString() + "</td></tr>");
	    out.println("<tr><th>Pearson In Charge</th><td>" + course.get("courseSecretariatName").toString() + "</td></tr>");
	    out.println("</table>");

	    out.println("<div class=\"text-center\">");
	    out.println("<form method=\"POST\">");
	    out.println("<input type=\"hidden\" name=\"courseID\" value=\"" + course.get("courseID").toString() + "\">");
	    out.println("<input type=\"hidden\" name=\"staffID\" value=\"1\">");
	    out.println("<a href=\"CourseCalendarForm\" class=\"btn btn-primary\">"
		    + "<i class=\"glyphicon glyphicon-chevron-left\"></i> Back"
		    + "</a> "
		    + "<button type=\"submit\" class=\"btn btn-success\">"
		    + "<i class=\"glyphicon glyphicon-send\"></i> Apply This Course"
		    + "</button>");

	    out.println("</form>");
	    out.println("</div>");

	    out.println("</div>");

	    request.getRequestDispatcher("/inc/footer.jsp").include(request, response);
	} catch (SQLException ex) {
	    Logger.getLogger(ApplicationForm.class.getName()).log(Level.SEVERE, null, ex);
	}

    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
	    throws ServletException, IOException {
	response.setContentType("text/html;charset=UTF-8");
	try (PrintWriter out = response.getWriter()) {
	    request.getRequestDispatcher("/inc/header.jsp").include(request, response);

	    boolean submited;
	    String courseID = request.getParameter("courseID");
	    String staffID = request.getParameter("staffID");

	    submited = Bussiness.Application.submitCourseApplication(Integer.parseInt(courseID), Integer.parseInt(staffID));

	    if (submited) {
		out.println("<div class=\"alert alert-success\">");
		out.println("<h4>Success Application</h4>");
		out.println("<p>Your application is successfully submited. Click here to see your application status.</p>");
		out.println("</div>");
	    } else {
		out.println("<div class=\"alert alert-danger\">");
		out.println("<h4>Something Happen</h4>");
		out.println("<p>Sorry, some error occurred. Please try again later...</p>");
		out.println("</div>");
	    }

	    request.getRequestDispatcher("/inc/footer.jsp").include(request, response);
	} catch (SQLException ex) {
	    Logger.getLogger(ApplicationForm.class.getName()).log(Level.SEVERE, null, ex);
	}
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
	return "Short description";
    }// </editor-fold>

}
