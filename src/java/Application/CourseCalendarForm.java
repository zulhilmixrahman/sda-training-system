package Application;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import Bussiness.Course;
import java.sql.SQLException;
import java.util.Iterator;
import java.util.Map;

/**
 *
 * @author ahmadzulhilmi
 */
public class CourseCalendarForm extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
	    throws ServletException, IOException {
	response.setContentType("text/html;charset=UTF-8");
	try (PrintWriter out = response.getWriter()) {
	    request.getRequestDispatcher("/inc/header.jsp").include(request, response);

	    out.println("<div class=\"col-md-offset-1 col-md-10\">");
	    out.println("<h3>Course Training Calendar</h3>");
	    out.println("<hr />");

	    out.println("<div class=\"table-responsive\">");
	    out.println("<table class=\"table table-bordered table-condensed table-hover table-striped\">");
	    out.println("<thead>");
	    out.println("<tr>");
	    out.println("<th class=\"text-center\">#</th>");
	    out.println("<th class=\"text-center\">Course/Training</th>");
	    out.println("<th class=\"text-center\">Start Date</th>");
	    out.println("<th class=\"text-center\">End Date</th>");
	    out.println("<th class=\"col-md-2 text-center\">Action</th>");
	    out.println("</tr>");
	    out.println("</thead>");
	    out.println("<tbody>");

	    int loopItem = 0;
	    Map courseMap = Course.getAllCourse();
	    // Loop through records
	    for (Iterator it1 = courseMap.entrySet().iterator(); it1.hasNext();) {
		Map.Entry<Integer, Map> course = (Map.Entry<Integer, Map>) it1.next();
		// Get Array Data
		Map values = course.getValue();

		out.println("<tr>");
		out.println("<td>" + (loopItem + 1) + "</td>");
		out.println("<td>" + values.get("courseName").toString() + "</td>");
		out.println("<td>" + values.get("courseStartDate").toString() + "</td>");
		out.println("<td>" + values.get("courseEndDate").toString() + "</td>");
		out.println("<td class=\"text-center\">");
		out.println("<a href=\"ApplicationForm?id=" + values.get("courseID").toString() + "\" class=\"btn btn-success\">View Details</a>");
		out.println("</td>");
		out.println("</tr>");

		loopItem++;
	    }

	    out.println("</tbody>");
	    out.println("</table>");
	    out.println("</div>");
	    
	    out.println("</div>");

	    request.getRequestDispatcher("/inc/footer.jsp").include(request, response);
	} catch (SQLException ex) {
	    System.out.println("Error: " + ex.getMessage());
	}
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
	    throws ServletException, IOException {
	processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
	    throws ServletException, IOException {
	processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
	return "Short description";
    }// </editor-fold>

}
