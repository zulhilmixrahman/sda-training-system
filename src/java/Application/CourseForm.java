package Application;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author ahmadzulhilmi
 */
public class CourseForm extends HttpServlet {

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
	    throws ServletException, IOException {
	response.setContentType("text/html;charset=UTF-8");
	try (PrintWriter out = response.getWriter()) {
	    request.getRequestDispatcher("/inc/header.jsp").include(request, response);

	    out.println("<div class=\"col-md-offset-1 col-md-10\">");
	    out.println("<h3>New Course Form</h3>");
	    out.println("<hr />");

	    out.println("<form method=\"POST\">");

	    out.println("<div class=\"form-group\">");
	    out.println("<label>Course Title</label>");
	    out.println("<input type=\"text\" name=\"courseName\" class=\"form-control\">");
	    out.println("</div>");
	    out.println("<div class=\"form-group\">");
	    out.println("<label>Start Date</label>");
	    out.println("<input type=\"text\" name=\"courseStartDate\" class=\"form-control dateField\">");
	    out.println("</div>");
	    out.println("<div class=\"form-group\">");
	    out.println("<label>End Date</label>");
	    out.println("<input type=\"text\" name=\"courseEndDate\" class=\"form-control dateField\">");
	    out.println("</div>");
	    out.println("<div class=\"form-group\">");
	    out.println("<label>Secretariat</label>");
	    out.println("<select name=\"courseSecretariatID\" id=\"select\" class=\"form-control\">");
	    out.println("<option value=\"\">-- Choose Secretariat --</option>");
	    out.println("<option value=\"1\">AAAA</option>");
	    out.println("</select>");
	    out.println("</div>");
	    out.println("<div class=\"form-group text-center\">");
	    out.println("<button type=\"submit\" class=\"btn btn-primary btn-block\">Add New Course</button>");
	    out.println("</div>");
	    out.println("</form>");

	    out.println("</div>");

	    request.getRequestDispatcher("/inc/footer.jsp").include(request, response);
	}
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
	    throws ServletException, IOException {
	try (PrintWriter out = response.getWriter()) {
	    request.getRequestDispatcher("/inc/header.jsp").include(request, response);

	    boolean isValidForm = false;
	    if (!request.getParameter("courseName").isEmpty()
		    && !request.getParameter("courseStartDate").isEmpty()
		    && !request.getParameter("courseEndDate").isEmpty()
		    && !request.getParameter("courseSecretariatID").isEmpty()) {
		isValidForm = true;
	    } else {
		out.println("<div class=\"alert alert-danger\">");
		out.println("<h4>Validation Fail</h4>");
		out.println("<p>Please provide all field!</p>");
		out.println("</div>");
	    }

	    if (isValidForm) {
		String courseName = request.getParameter("courseName");
		String courseStartDate = request.getParameter("courseStartDate");
		String courseEndDate = request.getParameter("courseEndDate");
		int courseSecretariatID = Integer.parseInt(request.getParameter("courseSecretariatID"));
		
		boolean submited = Bussiness.Course.addNewCourse(courseName, courseStartDate, courseEndDate, courseSecretariatID);
		
		if (submited) {
		    out.println("<div class=\"alert alert-success\">");
		    out.println("<h4>Success Add New Course</h4>");
		    out.println("<p>New course successfully added.</p>");
		    out.println("</div>");
		} else {
		    out.println("<div class=\"alert alert-danger\">");
		    out.println("<h4>Something Happen</h4>");
		    out.println("<p>Sorry, some error occurred. Please try again later...</p>");
		    out.println("</div>");
		}
	    }

	    request.getRequestDispatcher("/inc/footer.jsp").include(request, response);
	} catch (SQLException | ParseException ex) {
	    Logger.getLogger(CourseForm.class.getName()).log(Level.SEVERE, null, ex);
	}
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
	return "Short description";
    }// </editor-fold>

}
