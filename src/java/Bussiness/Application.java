package Bussiness;

import Middleware.ApplicationDB;
import java.sql.SQLException;
import java.util.Map;

/**
 *
 * @author ahmadzulhilmi
 */
public class Application {

    public Application() {
    }
    
    public static boolean submitCourseApplication(int courseID, int staffID) throws SQLException{
	return ApplicationDB.saveCourseApplication(courseID, staffID);
    }
    
    public static Map getAllApplicationToAcknowledge() throws SQLException{
	return ApplicationDB.getAllApplicationToAcknowledge();
    }
    
    public static boolean acknowledgeApplication(int applicationID, int status) throws SQLException{
	return ApplicationDB.acknowledgeApplication(applicationID, status);
    }
    
}
