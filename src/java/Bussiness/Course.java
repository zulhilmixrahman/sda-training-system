package Bussiness;

import Middleware.CourseDB;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

/**
 *
 * @author ahmadzulhilmi
 */
public class Course {
    
    private int index, courseID, courseSecretariatID;
    private String courseName, courseStartDate, courseEndDate;

    public Course() {
    }

    public static Map getAllCourse() throws SQLException {
	return CourseDB.getAllApplication();
    }

    public static Map getCourseDetails(int id) throws SQLException {
	return CourseDB.getCourseDetails(id);
    }

    public static boolean addNewCourse(String courseTitle, String startDate, String endDate, int secretariatID) throws SQLException, ParseException {
	startDate = Course.dateConvertToMySqlFormat(startDate);
	endDate = Course.dateConvertToMySqlFormat(endDate);
	return CourseDB.addNewCourse(courseTitle, startDate, endDate, secretariatID);
    }

    public static String dateConvertToMySqlFormat(String date) {
	try {
	    DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
	    Date stringDate = df.parse(date);

	    DateFormat sqlf = new SimpleDateFormat("yyyy-MM-dd");
	    String sqlDate = sqlf.format(stringDate);
	    return sqlDate;
	} catch (ParseException ex) {
	    return ex.getMessage();
	}

    }

}
