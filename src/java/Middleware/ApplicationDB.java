package Middleware;

import java.sql.*;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author ahmadzulhilmi
 */
public class ApplicationDB {

    public ApplicationDB() {
    }

    public static void getAllApplication() throws SQLException {
	Statement statement = DbConnection.getConnection().createStatement();

	String query = "SELECT a.applicationID, s.staffID, s.staffName, c.courseID, c.courseName, a.applicationDate "
		+ "FROM Application AS a "
		+ "JOIN Staff AS s ON a.staffID = s.staffID "
		+ "JOIN Course AS c ON a.courseID = c.courseID ";
	System.out.println(query);
	ResultSet resultSet = statement.executeQuery(query);

	if (resultSet.next()) {
	    System.out.println(resultSet.getString("applicationID") + "<br />");
	}
    }

    public static boolean saveCourseApplication(int courseID, int staffID) throws SQLException {
	String query = "INSERT INTO Application (staffID, courseID, applicationDate, attendanceStatus) VALUES (?, ?, NOW(), 0)";
	PreparedStatement pStatement = DbConnection.getConnection().prepareStatement(query);
	pStatement.setInt(1, staffID);
	pStatement.setInt(2, courseID);
	int execute = pStatement.executeUpdate();

	if (execute == 1) {
	    return true;
	} else {
	    return false;
	}
    }
    
    public static Map getAllApplicationToAcknowledge() throws SQLException{
	Map<Integer, Map> items = new HashMap<>();
	
	String query = "SELECT a.applicationID, DATE_FORMAT(a.applicationDate, '%d %M %Y') AS applicationDate, "
		+ "c.courseName, s.staffName, a.acknowledgeStatus "
		+ "FROM Application AS a "
		+ "JOIN Course AS c ON a.courseID = c.courseID "
		+ "JOIN Staff AS s ON a.staffID = s.StaffID ";
	Statement statement = DbConnection.getConnection().createStatement();
	ResultSet resultSet = statement.executeQuery(query);
	
	int loopResult = 0;
	while (resultSet.next()) {
	    Map<String, String> item = new HashMap<>();
	    item.put("applicationID", resultSet.getString("applicationID"));
	    item.put("courseName", resultSet.getString("courseName"));
	    item.put("staffName", resultSet.getString("staffName"));
	    item.put("applicationDate", resultSet.getString("applicationDate"));
	    
	    int acknowledgeStatus = resultSet.getInt("acknowledgeStatus");
	    String acknowledgeTxt = "";
	    if(acknowledgeStatus == 1)
		acknowledgeTxt = "Accepted";
	    else if (acknowledgeStatus == 2)
		acknowledgeTxt = "Rejected";
		
	    item.put("acknowledgeStatus", resultSet.getString("acknowledgeStatus"));
	    item.put("acknowledgeTxt", acknowledgeTxt);
	    
	    items.put(loopResult, item);
	    loopResult++;
	}
	return items;
    }
    
    public static boolean acknowledgeApplication(int applicationID, int status) throws SQLException{
	String query = "UPDATE Application SET acknowledgeStatus = ? WHERE applicationID = ? ";
	PreparedStatement pStatement = DbConnection.getConnection().prepareStatement(query);
	pStatement.setInt(1, status);
	pStatement.setInt(2, applicationID);
	int execute = pStatement.executeUpdate();
	
	if (execute == 1) {
	    return true;
	} else {
	    return false;
	}
    }

}
