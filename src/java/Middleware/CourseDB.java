package Middleware;

import java.sql.*;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author ahmadzulhilmi
 */
public class CourseDB {

    public CourseDB() {}
    
    public static Map getAllApplication() throws SQLException {
	Map<Integer, Map> items = new HashMap<>();
	
	String query = "SELECT c.courseID, cs.courseSecretariatName, c.courseName, c.courseStartDate, c.courseEndDate "
		+ "FROM Course AS c "
		+ "JOIN CourseSecretariat AS cs ON c.courseSecretariatID = cs.courseSecretariatID ";
	Statement statement = DbConnection.getConnection().createStatement();
	ResultSet resultSet = statement.executeQuery(query);

	int loopResult = 0;
	while (resultSet.next()) {
	    Map<String, String> item = new HashMap<>();
	    item.put("courseID", resultSet.getString("courseID"));
	    item.put("courseName", resultSet.getString("courseName"));
	    item.put("courseStartDate", resultSet.getString("courseStartDate"));
	    item.put("courseEndDate", resultSet.getString("courseEndDate"));
	    item.put("courseSecretariatName", resultSet.getString("courseSecretariatName"));
	    
	    items.put(loopResult, item);
	    loopResult++;
	}
	return items;
    }
    
    public static Map getCourseDetails(int id) throws SQLException{
	String query = "SELECT c.courseID, cs.courseSecretariatName, c.courseName, c.courseStartDate, c.courseEndDate "
		+ "FROM Course AS c "
		+ "JOIN CourseSecretariat AS cs ON c.courseSecretariatID = cs.courseSecretariatID "
		+ "WHERE c.courseID = ?";
	
	PreparedStatement pStatement = DbConnection.getConnection().prepareStatement(query);
	pStatement.setInt(1, id);
	ResultSet resultSet = pStatement.executeQuery();
	
	Map<String, String> item = new HashMap<>();
	if (resultSet.first()) {
	    item.put("courseID", resultSet.getString("courseID"));
	    item.put("courseName", resultSet.getString("courseName"));
	    item.put("courseStartDate", resultSet.getString("courseStartDate"));
	    item.put("courseEndDate", resultSet.getString("courseEndDate"));
	    item.put("courseSecretariatName", resultSet.getString("courseSecretariatName"));
	}
	return item;
    }
    
    public static boolean addNewCourse(String courseTitle, String startDate, String endDate, int secretariatID) throws SQLException{
	String query= "INSERT INTO Course (courseSecretariatID, courseName, courseStartDate, courseEndDate) VALUES (?, ?, ?, ?)";
	
	PreparedStatement pStatement = DbConnection.getConnection().prepareStatement(query);
	pStatement.setInt(1, secretariatID);
	pStatement.setString(2, courseTitle);
	pStatement.setString(3, startDate);
	pStatement.setString(4, endDate);
	int execute = pStatement.executeUpdate();

	if (execute == 1) {
	    return true;
	} else {
	    return false;
	}
    }

}
