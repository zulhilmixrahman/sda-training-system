package Middleware;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author ahmadzulhilmi
 */
public class DbConnection {

    public DbConnection() {
	
    }
    
    public static Connection getConnection(){
	try {
	    // MySQL Connection Setting
	    String DbUser = "root";		// MySQL Username
	    String DbPass = "password";		// MySQL Password
	    String DbName = "training_system";	// MySQL Database Name

	    // MySQL Driver
	    String driver = "com.mysql.jdbc.Driver";
	    
	    // MySQL Connection Url
	    String URL = "jdbc:mysql://localhost:3306/" + DbName;
	    Class.forName(driver);

	    // Establish network connection to database.
	    Connection connection = DriverManager.getConnection(URL, DbUser, DbPass);
	    return connection;
	} catch (ClassNotFoundException cnfe) {
	    System.err.println("Error loading driver: " + cnfe);
	} catch (SQLException sqle) {
	    System.err.println("Error connecting: " + sqle);
	}
	
	return null;
    }

}
