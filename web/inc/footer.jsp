<%-- 
    Document   : header
    Created on : Apr 14, 2016, 9:19:18 PM
    Author     : ahmadzulhilmi
--%>
</div>
<script type="text/javascript" src="frontend/js/jquery-1.12.3.min.js"></script>
<script type="text/javascript" src="frontend/js/bootstrap.min.js"></script>
<script type="text/javascript" src="frontend/datepicker/js/bootstrap-datepicker.min.js"></script>
<script>
    $(function () {
	$.fn.datepicker.defaults.format = "dd-mm-yyyy";
	$('.dateField').datepicker({
	    autoclose: true
	});
    });
</script>
</body>
</html>
