<%-- 
    Document   : index
    Created on : Apr 14, 2016, 9:18:25 PM
    Author     : ahmadzulhilmi
--%>

<%@include file="inc/header.jsp" %>

<%@ page import="java.io.*,java.util.*,java.sql.*"%>
<%@ page import="javax.servlet.http.*,javax.servlet.*" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql"%>

<sql:setDataSource var="training" driver="com.mysql.jdbc.Driver"
		   url="jdbc:mysql://localhost/training_system"
		   user="root" password="password"/>

<sql:query dataSource="${training}" var="courses">
    SELECT * FROM Course;
</sql:query>

<h3>Course Calendar</h3>
<hr />
<div class="table-responsive">
    <table class="table table-bordered table-condensed table-hover table-striped">
	<thead>
	    <tr>
		<th class="text-center">#</th>
		<th class="text-center">Course/Training</th>
		<th class="text-center">Start Date</th>
		<th class="text-center">End Date</th>
		<th class="col-md-2 text-center">Action</th>
	    </tr>
	</thead>
	<tbody>
	    <c:forEach var="row" items="${courses.rows}">
		<tr>
		    <td></td>
		    <td><c:out value="${row.courseName}"/></td>
		    <td><c:out value="${row.courseStartDate}"/></td>
		    <td><c:out value="${row.courseEndDate}"/></td>
		    <td class="text-center">
			<a href="#" class="btn btn-primary">View</a>
			<a href="#" class="btn btn-success">Apply</a>
		    </td>
		</tr>
	    </c:forEach>
	</tbody>
    </table>
</div>

<%@include file="inc/footer.jsp" %>